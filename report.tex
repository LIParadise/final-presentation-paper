\documentclass[11pt]{article}
\usepackage{amsthm}
\usepackage{amsfonts}
\usepackage[toc,page]{appendix}
\usepackage[style=ieee]{biblatex}
\usepackage{./custom}
\usepackage{./abbr}
\usepackage{./code_style}
\addbibresource{FFFFFF.bib}

\pagestyle{plain}

\title{Final Report - A Short Survey on Applications of Kolmogorov Complexity}
\author{Chi-Tse Huang, Chu-Han Chen}
\date{B06901168, B05901105}

\begin{document}

\maketitle

\begin{abstract}
    The concept of shortest description length of an object stemmed in last century, by Chaitin, Solomonoff, and Kolmogorov\cite{KOLMOGOROV1998387}, also known as Kolmogorov complexity or algorithmic complexity. Despite its elegance and nice properties, its uncomputability and additive constant factors hinder its practical adaptions, and to date people tend to use stochastic based information theory, as proposed by Shannon, to capture the intuitive notion of complexity.
    
    In this survey, we'll review some properties of algorithmic complexity, methods to approximate algorithmic complexity, in particular \textit{Coding Theorem Method} (\(\ctm\) for short) and \textit{Block Decompsition Method} (\(\bdm\) for short), their performance and theoretical bounds, and finally do some simple experiments on lossless compression algorithms as estimators for algorithmic complexity.
\end{abstract}

\keywords{Algorithmic Complexity, Block Decomposition Method (BDM), Coding Theorem Method (CTM), Lempel–Ziv–Markov Chain Algorithm (LZMA), Lempel-Ziv-Welch Algorithm (LZW)}

\section{Introduction}\label{ch:introduction}
    Stochastic approach for information processing had been proven successful. As per proposed by Shannon, the framework assumes existence of stochastic process acting as information source, whilst the objective be to transmit the information, either losslessly or within some distortion constraint, over a lossy channel to a receiver. The framework adapts well in many fields, asserting the insight of both Shannon's and the measure based probability theory of Kolmogorov. In such a framework, complexity of stationary ergodic source is captured by its entropy rate.
    
    Yet some found need of distinct descriptions on complexity. In Shannon's framework, a message is assumed to be produced by a source which obeys some probability law, and hence complexity measures in the framework such as entropy rate is more describing behavior of the source, instead of any empirical instance. This may not be desired result, as one may instead aim to find suitable measure of complexity for a fixed, concrete object, such as a piece of poem, a DNA sequence, or a network topology graph.
    
    Another reason for seeking complexity measure other than Shannon entropy is when objects have inherently many features. In that case, as pointed out in \cite{Lecture_AlgorithmicInformationDynamics}\cite{complexityexplorer_2018}, for network graphs, what features are chosen when defining Shannon style entropy become important; entropy become a feature-dependent measure, and hence entropy calculated based on different features may vary, making the characterization unstable and hence may be desired to be avoided.
    
    Algorithmic complexity circumvents the issues above. Given any universal computing model, the algorithmic complexity is well-defined and depend solely on the object under consideration; algorithmic complexity describes the object and the object only, rather than describing behaviors of underlying source, which is often impossible to gain full knowledge of, as entropy rate in Shannon's framework. And as shown by invariance theorem, it's computing model agnostic up to constants, meaning algorithmic complexity is indeed intrinsic property of object under consideration. By virtue of being an intrinsic description, algorithmic complexity is invariant, as opposed to Shannon entropy variants, which often depend on specific feature chosen.
    
    In section \ref{ch:Preliminaries}, we recap basic properties and short-comings of algorithmic complexity. In section \ref{ch:CTM-BDM}, we survey methods of approximating algorithmic complexity, \(\ctm\) and \(\bdm\), following \cite{DBLP:journals/corr/ZenilSKHR16}\cite{DBLP:journals/corr/abs-1211-1302}\cite{DBLP:journals/corr/abs-1101-4795}. In section \ref{ch:exp}, we give some simple experiment results as empirical evidence of deficiency of lossless algorithms as algorithmic complexity estimators.

\section{Preliminaries}\label{ch:Preliminaries}
    
    Let \(\mathbb{N}\) denote the natural numbers, i.e. \( \{0,1,2,\cdots\} \). A binary string \(b_0 b_1 b_2 b_3 b_4 \cdots b_n\) and its natural number correspondent would be treated equally and interchangeably, namely the natural number \(m =
    \sum\limits_{i \in \mathbb{N} ,~  i\leq n}{
        2^{b_i}
    }\).
    
    \subsection{Algorithmic Complexity}\label{ch:Preliminaries-Alg-C}

    In simplest terms, arithmetic complexity of an object \(x\) is defined to be the length of the shortest program which halts with \(x\) as output. For instance (appendix \ref{appendix:host_wsl_machine}):
    \begin{lstlisting}[style=CPP]
    #include <iostream>
    int main(){
        using namespace std;
        cout << "Hello, World" << endl;
        return 0;
    }
    \end{lstlisting}
    The binary produces string \(\text{Hello, World}\), but it is no where near the minimum in size. Many factors are in play: compiler version, host machine instruction set, kernel and $\text{C++}$ library version, compile options, or one may use $\text{C}$, $\text{Rust}$, or any other programming languages. However this simple example hints us the way to formally define algorithmic complexity. First, a universal computing model would be required. According to the Church-Turing Thesis, the rather simple Turing machine following chapter \(14\) in \cite{CoverThomas-10.5555/1146355} and \cite{Jerrold-Siegel-UMSL-lecture-note} suits.
    
    \begin{definition}[3 tape Turing machine]\label{def:tm}
        A Turing machine \(\tm\) is a tuple of tuples, \(\left(
        p,
        \left( Q, q, A, S, R, W, O, q_0 \right)
        \right)\), where \begin{align*}
            p   & \coloneqq B{\left( \{0,1\} \right)}^{*}\text{, a finite length binary string preceded by the }\textit{blank}\text{ symbol }B\in A \\
            A   & \coloneqq \{0, 1, B\} \\
            Q   & \coloneqq \text{A finite set} \\
            q   & \coloneqq \text{An element in }Q \\
            S   & \coloneqq \{ W_\text{right}, W_\text{left},           W_\text{stay} \}
                \times A
                \times\{ O_\text{right}, O_\text{stay} \}
                \times A \\
            R   & \coloneqq \text{Partial function }{
                \left(Q \times A \times A\right) \to
                \left(Q \times S\right)} \\
            O   & \coloneqq \text{Infinite long array of the blank symbol }B \\
            W   & \coloneqq \text{Infinite long array of the blank symbol }B \\
            q_0 & \coloneqq \text{An element in }Q
        \end{align*}
    \end{definition}
    
    Working of the Turing machine is illustrated as follows: let time be discrete, and there exists \(2\) infinitely long tape to write to (\(O, W\)), \(\textit{output}\) tape and \(\textit{work}\) tape, respectively. Suppose a personnel Alice wants to calculate something, taking advantage of the infinite memory. At any single time, Alice reads exactly \(1\) bit of \(p\) and immediately discards it from \(p\), and read exactly \(1\) bit from \(\textit{work}\) tape. To make no ambiguities, Alice prepares a set of mental states (\(Q\)) and a set of rules (\(R\)), regulating itself be in exactly \(1\) mental state (\(q\)) at a time and execute that instructed by the mapping \(R\): suppose bit read from \(p\) be \(p_i\), bit read from work tape be \(w_i\), current mental state be \(q\), and \(R\left( q, p_i, w_i \right) = (q^\prime, w, w_a, o, o_a )\), then Alice changes mental state to \(q^\prime\), and writing to and/or moving the working tape and output tape according to \((w, w_a, o, o_a)\). If Alice stops calculation, the Turing machine is said to \( \textit{halt} \) on the input string \(p\), or \(\tm\) \( \textit{accepts} \) \(p\); in this case the string \(s\) on the output tape is said to be the \(\textit{output}\), denoted by \(\tm{p}={s}\). If Alice don't stop calculating, \(p\) is said to be \(\textit{rejected}\) by \(\tm\). At the beginning, Alice starts with mental state \(q_0\).
    
    For simplicity, in the following sections, we abuse notation and denote \(\tm = \left( Q, q, A, S, R, q_0 \right)\) as a Turing machine, and denote the tuple introduced in definition \ref{def:tm} simply as \( \left( \tm, p \right) \). Subsequently, for different Turing machines \(\tm_1 =
    \left( Q_1, q_1, A_1, S_1, R_1, q_{0_1} \right)
    \) and \(\tm_2 =
    \left( Q_2, q_2, A_2, S_2, R_2, q_{0_2} \right)
    \), we say \(\tm_1 = \tm_2\) if \(
    \left(
    i_1, Q_1, q_1, A_1, S_1, R_1
    \right) =
    \left(
    i_1, Q_1, q_1, A_1, S_1, R_1
    \right) \). We also abuse notation on the input: suppose \(p\in{\{0,1\}}^{*}\), we consider \(Bp\) and \(p\) equal, and would be used interchangeably. Common notion follows:
    
    \begin{definition}[Language of Turing machine]\label{def:language}
        Language of a Turing machine \(\tm\) is the following set of binary strings:
        \[
            \{
                x \in {\{0,1\}}^{*}
                ~\big\vert~
                \tm\text{ accepts }{x}
            \}
        \]
    \end{definition}
    
    \begin{definition}[Computable]\label{def:computable}
        A partial function \(f:
            {\{0,1\}}^{*} \to {\{0,1\}}^{*}
        \) is said to be \textit{computable} if there's a Turing machine \(\tm\) accepting exactly the domain of \(f\) and \(\tm{x} = f(x)\text{ for all }x\text{ in domain of }f\).
    \end{definition}
    
    \begin{definition}(Self delimiting encode)\label{def:SelfDelimit}
        As defined in \cite{10.5555/1478784}, chapter \(1.4\) and exercise \(1.7.2\), a level-\(n\) self delimiting encode for binary string \(x\), \(E_n{\left(x\right)}\), is defined as: \[
        E_n{\left(x\right)} \coloneqq 
            \begin{cases}
                    1^{x}0 & \text{if } n = 0\\
                    E_{n-1}\left( l(x) \right)x, & \text{if }n \geq 0
            \end{cases}
        \]
        where \(l(x)\) denote length of string \(x\). Further,
        \[
        E_n\left( x_1, x_2, \cdots, x_n \right) \coloneqq
        \begin{cases}
            E_n\left( x_1, E_n( x_2, x_3, \cdots, x_n ) \right), &\text{if }n\geq 3 \\
            E_n(x_1)x_2, &\text{if }n=2
        \end{cases}
        \]
    \end{definition}
    
    For instance, \(E_1{\left( 101 \right)} = 1110101 \). Definition \ref{def:SelfDelimit} suggests an encoding in the form of general recursive function, which is essential to Turing machines, as illustrated in the following theorems. Unless stated otherwise, self delimiting encode refer to \(E_1\) in the following. Notice as shown in \cite{10.5555/1478784}, \(E_1\) is a prefix-free encoding. Immediately, we have the following lemma:
    
    \begin{lemma}[Self delimiting encoding of Turing machines]\label{lemma:encode-TM}
        Turing machines can be encoded using self delimiting encode
    \end{lemma}
    
    \begin{theorem}[Turing machine have same computability as general recursive function]\label{thm:TM-equal-general-recursive}
        There exists equivalence relation between the set of general recursive functions and Turing machines, where the equivalence relation is such that when general recursive function \(f\) is equivalent to Turing machine \(\tm\), for any binary string \(x\), \(x\) in domain of \(f\) iff \(\tm\) accepts \(x\) and \(f(x)\) = \(\tm{x}\), and \(x\) not in domain of \(f\) iff \(\tm\) on input \(x\) never halts, i.e. \(x\) not in language of \(\tm\).
    \end{theorem}
    
    \begin{lemma}\label{lemma:empty-input-for-TM-is-enough}
        Given binary string \(x\), that there exists Turing machine \(\tm^\prime\) and non-empty binary string \(p\) such that \(\tm^\prime{p}=x\) implies there exists Turing machine \(\tm\) such that \(\tm{\varepsilon}=x\)
    \end{lemma}
    
    Intuitively, this theorem describes the fact one can store constants in programs, which is common in practice. A function on input \(10\) giving \(\pi\) to the \(10\)-th decimal produces is, on this exact input, the same as a function that just returns \(3.1415926535\) on empty input.
    
    \begin{theorem}[Computable bijective relation on natural number and Turing machines]\label{thm:Turing-machine-computable-N}
    There exists computable bijective relation between Turing machines and \(\mathbb{N}\)
    \end{theorem}
    
    Intuitively, this theorem holds since one can use the bijection given by the self delimiting encoding promised by lemma \ref{lemma:encode-TM}. By virtue of theorem \ref{thm:Turing-machine-computable-N}, we can uniquely identify a Turing machine using a natural number \(n\); in this case we also denote the machine \(\tm_n\).
    
    \begin{theorem}[Language of a Turing machine form prefix-free set]\label{thm:prefix-free-language}
        Given Turing machine \(\tm\), its language is prefix-free.
    \end{theorem}
    
    Proof is straightforward under the 3-tape setup in definition \ref{def:tm}, since the input is read-only.
    
    Observe we can actually encode the tuple \(\left( \tm, p \right)\) with the multi-input version self delimiting encode as defined in \ref{def:SelfDelimit}
    
    \begin{theorem}[Existence of universal Turing machine]\label{thm:utm}
    There exists Turing machine \(\utm\), such that when provided self delimiting encode of \(\left(
    n, p
    \right)\) where \(n\in\mathbb{N}\) and \(p\in{\{0,1\}}^{*}\), \(
        \utm{n,p}
    \) mimics exactly the behavior of Turing machine \(\tm_n\) with input \(p\).
    \end{theorem}
    
    With help of universal Turing machine, we are now ready for formal definition of algorithmic complexity.
    
    \begin{definition}[Algorithmic complexity based on universal Turing machine]\label{def:kcu}
    Given universal Turing machine $\utm$, string \(x\), define algorithmic complexity of \(x\) based on $\utm$ be \(\kcu{x}\), where:
        \begin{equation*}
            \kcu{x}\coloneqq
            \min\limits_{\utm{p}=x}{
                \{ \lvert p \rvert \}
            }
        \end{equation*}
    \end{definition}
    
    A example of definition \ref{def:kcu}, which encompasses self-delimiting encode of Turing machines, is the following lemma, which fits the intuition that the longest program producing some string is not much larger than the string itself, since one can hard-code it in the program:
    
    \begin{lemma}[Algorithimic complexity is approximately bounded by the length of the string]\cite{CoverThomas-10.5555/1146355}\label{lemma:kc-limited-by-length-of-string}
        \[
            \kcu{s} \leq l(s) + 2\cdot \lg{\left(l(s)\right)} + c_\mathcal{U}
        \]
    \end{lemma}
    
    The proof idea came from properties of self-delimiting encode \ref{def:SelfDelimit}; in particular the term $2\cdot \lg{\left(l(s)\right)}$ can be improved to $\lg^{*}{\left(l(s)\right)}$, with application of iterative self-delimiting encode.

    In virtue of theorem theorem \ref{thm:utm}, which states a universal Turing machine \(\utm\) can simulate any other Turing machine, including other universal Turing machine \(\utm^\prime\), and the aid of self delimiting encoding, one arrive at the following fundamental theorem to algorithmic complexity:
    
    \begin{theorem}[Invariance theorem]\label{thm:invariance}
        Given universal Turing machines, $\utm$ and $\utm^\prime$, there exists a constants \(n \in \mathbb{N}\) such that for all \(x\in{\{0,1\}}^{*}\), the following holds:\begin{equation*}
            \begin{split}
                \kcu{x} &
                \leq \kc_{\mathcal{U}^\prime}{x} + n\\
                \kc_{\mathcal{U}^\prime}{x} &
                \leq \kcu{x} + n
            \end{split}
        \end{equation*}
    \end{theorem}
    
    Notice the constant depend on only the choices of universal Turing machine, i.e. \(\utm\) and \(\utm^{\prime}\), and don't depend on any input string. The theorem also implies the algorithmic complexity indeed measure an intrinsic property of given binary string. In the following, we'll drop the subscript \(\mathcal{U}\) in \(\kcu\), since there's only a constant offset for any universal Turing machine.

    \begin{theorem}[Algorithmic complexity is uncomputable]\label{thm:uncomputable}
        There exists no total universal recursive function mapping any binary string \(x\) to corresponding \(\kc{x}\).
    \end{theorem}
    
    By theorem \ref{thm:TM-equal-general-recursive}, theorem \ref{thm:uncomputable} can also be stated as non-existence of Turing machine of which language is \(\{{\left(\{0,1\}\right)}^{*}\}\) and $\tm{x} = \kc{x}$. The proof can be done in a similar approach to Berry's paradox: suppose existence of algorithmic complexity oracle Turing machine \(\tm_{O}\), we can construct another Turing machine \(\tm\), with \(\tm_O\) as subroutine, that on empty input iterates through all binary strings, checks the algorithmic complexity, output first of which above \(n\), where \(n \geq \tm_{O}{\tm}\). The construction is always feasible, since \(n\) grow faster than \(\lg{\left(n\right)}\), and length of self delimiting encode of \(n\) is \(\mathcal{O}{\left(n\right)}\). The construction leads to obvious contradiction, since the output string of \(\tm\) should have algorithmic complexity \(n\), as promised by oracle \(\tm_{O}\), but is in this case produced by \(\tm\), where \(\tm_{O}{\tm}\leq n\)
    
    Theorem \ref{thm:invariance} and \ref{thm:uncomputable} introduces hindrance to practical applications of algorithmic complexity. Theorem \ref{thm:invariance} says for any computing model, there's a additive offset to the complexity obtained, which is potentially large. Theorem \ref{thm:uncomputable} further states one can not calculate systematically and effectively exact value of algorithmic complexity, even though the computing model is specified. Before we survey how the difficulties is to be alleviated in section \ref{ch:CTM-BDM}, we should introduce more related concepts:
    
    \begin{definition}[Algorithmic probability]\label{def:algo_prob}
        The algorithmic probability \(\algp{s}\) of a string \(s\) is defined as the following sum: \[
            \algp{s} \coloneqq
            \sum\limits_{\utm{p}=s}{
                2^{-\lvert p \rvert}
            }
        \]
        Where \(p\) is self-delimiting encode of some Turing machine and its corresponding input.
    \end{definition}
    
    The algorithmic probability of some string \(s\) can be viewed as the probability the string is to be produced if input to universal Turing machine is generated according to $\text{i.i.d}$ $\text{Bernoulli}{\left( 0.5 \right)}$. The definition creates an analogy for algorithmic probability to algorithmic complexity as probability mass of some instance out of a stochastic source to Shannon entropy, as the following theorem illustrates:
    
    \begin{theorem}[Relationship between algorithmic probability and algorithmic complexity]\label{thm:algo-prob-and-complexity}
        Given some universal Turing machine, algorithmic complexity and algorithmic probability have the following relationship: (\cite{book-Chaitin}, chapter 2) \[
            \kcu{s} = -\lg{\left(
                \algp{s}
            \right)} + \mathcal{O}{\left(1\right)}
        \]
    \end{theorem}
    
    Another intuitive illustration on how the relation as in theorem \ref{thm:algo-prob-and-complexity} works is that if an string is to be generated by multiple Turing machines, then the string would probably have a simple, not complicated form, i.e. have low algorithmic complexity.\label{note:link-between-algp-and-kc}
    
    \subsection{Lossless Souce Coding}\label{ch:Preliminaries-lossless}
    
    \begin{definition}[Lempel-Ziv-Welch Algorithm]\label{def:lzw}
        Given a fixed finite alphabet, the Lempel-Ziv-Welch algorithm (abbr. \(\lzw\)) describes an adaptive variable length coding scheme for universal lossless compression, based on the LZ78 prefix-tree algorithm.
    \end{definition}
    
    Verbosely speaking, \(\lzw\) takes streaming input and builds upon it a corresponding dictionary dynamically. It searches for the longest string that's on the dictionary, say \(s = \{c_0, c_1, \cdots, c_n \}\), such that the successor character, say \(c_{n+1}\), makes the string \(sc_{n+1}\) absent in current dictionary. It then adds an entry \(sc_{n+1}\) to the dictionary, then continue processing the input stream, searching the longest string \(s^\prime = \{c_{n+1}, c_{n+2}, \cdots, c_{m}\}\) such that \(s^\prime\) is in dictionary, so on and so forth. The dictionary starts with the finite alphabet and a special terminating entry, which is appended at the end of the compressed data, notifying the end to the decoder.
    
    The decoder receives a string of dictionary entries ending with a special predetermined terminating entry. The only job for the decoder is to reconstruct the dictionary dynamically constructed by the encoder. This can be done, since the decoder knows exactly when a new entry is added to the dictionary. Hence with a streaming setup doing inverse operations of the encoder, the decoder can fully specify the dictionary.
    
    \begin{theorem}[Optimality of $\lzw$]\label{thm:optimality-lzw}
        $\lzw$ achieves optimality in certain stochastic sources, e.g. stationary ergodic sources, i.e. achieving entropy rate. (\cite{CoverThomas-10.5555/1146355}, theorem 13.5.3) 
    \end{theorem}
    
    In short, proof idea is \(2\) parts. One is that given input string length \(n\), the entry count in the dictionary is bounded, since the entries mean distinct strings, and short strings are low in quantity. The other is there's a natural bound on emitted sequences for any stochastic sources; the more distinct characters a emitted string have, the lower the supremum of probability mass of such string, based on Cauchy's Inequality. Therefore, the more often the input string repeats itself, the lower the bound on the length of the compressed data (self delimiting encode for entry is used); the less often the input string repeat itself, the less the probability such string is produced by the stochastic source. Thus the expected codeword length is bounded, and can be shown to converge to entropy rate for stationary ergodic source.
    
    \begin{definition}[Lempel-Ziv Markov chain algorithm]\label{def:lzma}
        Given a fixed finite alphabet, the Lempel-Ziv Markov chain algorithm (abbr. \(\lzma\)) describes an dictionary-based algorithm, comprising of techniques from the Deflate algorithm, range encode, and the LZ77 algorithm\cite{D.Salomon-Data-Compression-Ref-10.1007/978-1-84628-603-2}.
    \end{definition}
    
    Despite empirical high performance, both in terms of compression rate and encoder/decoder computing resource requirements, we didn't find complete proof on optimality of \(\lzma\) for stochastic source. There are however claims \cite{6726971} that optimality follow from that of \(\text{LZ77}\), of which optimality proof idea for the latter is quite straightforward \cite{CoverThomas-10.5555/1146355}, following the lemma of \ref{lemma:kac}.
    
    \begin{lemma}[Kac's Lemma on Stationary Ergodic Process]\label{lemma:kac}
        Suppose \(\mathbb{X}=\{\cdots, X_{-1}, X_0, X_1, X_2, \cdots \} \) be a stationary ergodic source, \(\xi\) be in alphabet of the process with non-zero probability mass. Then \[
            \mathbb{E}{ \bigg\{
                \max\limits_{j\leq 0}{ \big\{
                    X_{j}=\xi
                    ~\big\vert~
                    X_{i}\neq\xi ~\forall~ j < i < 0
                \big\} }
                ~\bigg\vert~
                X_{0} = \xi
            \bigg\} } = \frac{1}{\Pr{\left( \xi \right)}}
        \]
    \end{lemma}
    
    In layman's terms, lemma \ref{lemma:kac} states a symbol with probability \(p\) is expected to occur periodically with period \(\frac{1}{p}\). The proof of optimality of \(\text{LZ77}\) algorithm follows from the observation that its sliding-window permits control of long strings using lemma \ref{lemma:kac}.
    
\section{Coding Theorem Method and Block Decomposition Method}\label{ch:CTM-BDM}

    Intuitively one may circumvent uncomputability of algorithmic complexity via dovetailing all Turing machines with all possible input strings, but the problem again boils down to non-existence of algorithmic procedures determining if arbitrary \(\tm{p}\) is to halt. With theorem \ref{thm:algo-prob-and-complexity}, a workaround is to first try to approximate algorithmic probability. Lemma \ref{lemma:empty-input-for-TM-is-enough} and theorem \ref{thm:utm} hints that considering only universal Turing machine and self-delimiting encode of Turing machines modulo empty input would probably good enough, hence a measure is defined as follows, as an attempt to approximate algorithmic complexity \(\algp\): \cite{DBLP:journals/corr/ZenilSKHR16}\cite{DBLP:journals/corr/abs-1101-4795}
    
    \begin{definition}[Approximate measure to algorithmic probability]\label{def:measure-D}
        \[
            D_{\left(n,m\right)}{\left( s \right)}
            \coloneqq
            \dfrac
            {\lvert \tm \text{ have }n\text{ states }m\text{ alphabet and }
            \tm{\varepsilon} = s\rvert}
            {\lvert \tm \text{ have }n\text{ states }m\text{ alphabet and }\tm\text{ accepts }\varepsilon\rvert}
        \]
    \end{definition}
    
    Notice that algorithmic probability is an semi-measure, in that with self-delimiting encode for \(\utm\) and the fact there exists non-halting Turing machines, it follows Kraft's inequality \cite{CoverThomas-10.5555/1146355} that \(\sum\limits_{s}{\algp{s}} < 1\), hence algorithmic probability don't serve as probability measure, as opposed to that introduced in definition \ref{def:measure-D}. With theorem \ref{thm:algo-prob-and-complexity}, and the assumption that definition \ref{def:measure-D} leads to good approximation of algorithmic probability, one have the following definition, as an approximation for algorithmic complexity:
    
    \begin{definition}[Coding Theorem Method]\label{def:CTM}
        For string \(s\), state count \(n\), and size of alphabet \(k\), the coding theorem method relates the tuple to a scalar: \[
            \ctm{{s,n,m}} \coloneqq -\lg{ \left(
                D_{\left(n,m\right)}{
                    \left( s \right)
                }
            \right) }
        \]
    \end{definition}
    
    One can see immediately from theorem \ref{thm:algo-prob-and-complexity} that if $D_{\left(n,m\right)}{\left(s\right)}$ is indeed close to $\algp{s}$, then $\ctm$ should also be a good approximation to $\kc{s}$. A natural question is then how close $D_{\left(n,m\right)}{\left(s\right)}$ is to $\algp{s}$, and how to calculate $D_{\left(n,m\right)}$ for any string \(s\).
    
    The justification can be illustrated with the following observations. Observe that if we were able to determine the halt-ness and output of all Turing machines with no more than \(n\) states and no more than \(m\) elements in the alphabet, and further suppose $s$ is one of the strings produced, the Turing machine $\tm$ generating $s$, i.e. $\tm{\varepsilon}=s$, should be such that $\lvert \tm \rvert \simeq \kc{s}$, where \(\lvert \cdot \rvert\) measures the length of self-delimiting encode of some Turing machine. The intuition behind the observation is how $\kcu$ is defined; the only difference is here the input is limited to the empty string $\varepsilon$. Additionally, observe the set of Turing machines with alphabet $\mathcal{A}$ and $\left( n+1 \right)$ states is strictly more \textit{powerful} than the set of Turing machines with the same alphabet $\mathcal{A}$ and $n$ states, in the sense that all languages accepted by some Turing machine in the latter set is also accepted by some other Turing machine in the former set, in that one can always have an \textit{useless} state. Hence the calculation of \( D_{\left(n^\prime,m^\prime\right)} \) for all $n^\prime \leq n$ and $m^\prime \leq m$ can be reduced to only calculating $D_{\left(n,m\right)}$. Combined with the illustration above \ref{note:link-between-algp-and-kc}, and that all $D$ calculates is the empirical output frequency for $n$ state, $m$ alphabet Turing machines, the relationship between $D_{\left(n,m\right)}{\left(s\right)}$ and $\algp{s}$ should be easy to believe in, at least for the strings generated by some $n$ state, $m$ alphabet Turing machines.
    
    Though the relation between $D$ and $\algp$, since the amount of Turing machines with $n$ state and $m$ alphabet grows exponentially with both parameters, it's impractical to calculate $D_{\left(n,m\right)}$ for where corresponding Busy Beaver Turing machine is yet known. Hence comes the introduction of the block decomposition method:
    
    \begin{definition}[The block decomposition method]\cite{DBLP:journals/corr/ZenilSKHR16}\label{def:bdm}
        Given state count $n$, alphabet size $m$, string $s$, the block decomposition method assigns a scalar to the tuple $\left( s, l, o\right)$, by considering $\ctm$ of substring of $s$ with a moving window of which length is $l$ proceeding $o$ characters each time.
        \[
        \bdm{{s,l,o}} \coloneqq \sum\limits_{i}{ \left(
            \ctm{{s^{i},n,m}}
            + \lg{ \left( r_i \right) }
        \right)}
        \]
        Where the summation is over all distinct strings discovered with the given moving window setup, and the number $r_i$ denotes the number of times some string appear in $s$. The $\lg$ operator is present since if $\kc{s}$ is to be related to the strings discovered in the moving windows, one should specify the times some string is to repeat, and the number would have self-delimiting encode bounded by $\lg$.
    \end{definition}
    
    The intuition behind $\bdm$ is to mimic approaches of lossless compression like $\lzw$, i.e. moving window, and combine the results obtained with $\ctm$ to approximate algorithmic complexity for longer strings.
    
    \begin{example}[Simple example for block decomposition method]\cite{DBLP:journals/corr/ZenilSKHR16}\label{ex:bdm}
        Consider alphabet $\{0,1\}$, $s=010101010101010101$, a block decomposition $\bdm{{s,12,1}}$ over $2$ state Turing machines considers $\ctm{{\cdot, 2, 2}}$ of the following strings: \begin{align*}
            & 010101010101 \\
            & 101010101010 \\
            & 010101010101 \\
            & 101010101010 \\
            & 010101010101 \\
            & 101010101010 \\
            & 010101010101
        \end{align*}
    With $\ctm$ results calculated in \cite{DBLP:journals/corr/ZenilSKHR16}, we have: \begin{align*}
        & \ctm{{010101010101,2,2}} \simeq 26.991 \\
        & \ctm{{101010101010,2,2}} \simeq 26.991
    \end{align*}, we derive: \[
        \begin{split}
            \bdm{{010101010101010101,12,1}} &\simeq  26.991 + 26.991 + \lg{(3)} + \lg{(4)} \\
            &\simeq  57.566
        \end{split}
    \], which serve as an approximation for algorithmic complexity for the string.
    \end{example}
    
    Based on the relationship between $\kc$ and $\ctm$ discussed above, we have the following theorem:
    
    \begin{theorem}[Block decomposition method achieves algorithmic complexity in the best case, Shannon entropy in the worst case]\cite{DBLP:journals/corr/ZenilSKHR16}\label{thm:bdm-optimality}
        Given object description $s$, with abuse of notation where $\bdm{{s, \{s_i\}}}$ denote summation of $\ctm$ over specified partition $\{s_i\}$ which is \textit{fine} enough in that all elements are those $\ctm$ have already calculated, and $\xi$ be the number of possible ways assembling partition $\{s_i\}$, we have the following inequalities: \[
            \begin{split}
                \kc{s} & \leq \bdm{{s,\{s_i\}}} +   \mathcal{O}{\left(\lg^{2}{\left(\xi\right)}\right)} +
                \varepsilon \\
                \bdm{{s,\{s_i\}}} & \leq
                l(s, \{s_i\})\kc{s} +
                \mathcal{O}{\left( l(s, \{s_i\}) \lg{\left(l(s, \{s_i\})\right)}\right)} -
                \varepsilon
            \end{split}
        \], where $\varepsilon$ denotes the error induced in $\ctm$ over the partition $\{s_i\}$.
    \end{theorem}
    
    The proof follows similar idea in lemma \ref{lemma:kc-limited-by-length-of-string}, and that self-delimiting encode of $(X, \{x_i\})$ is bounded by $\lg{\left(\xi\right)}$\cite{DBLP:journals/corr/ZenilSKHR16}, which confirms the idea that given enough computing power, i.e. to be able to run all $n$ state $m$ symbol Turing machines, $\bdm$ achieves algorithmic complexity.
    
    Further, \cite{DBLP:journals/corr/ZenilSKHR16} showed that in particular for matrices, and if Shannon entropy is defined over empirical frequency of features in considered partition, we have the following:
    
    \begin{theorem}[Block decomposition method is no worse than Shannon entropy in terms of approximation to algorithmic complexity]\label{thm:bdm-no-worse-than-entropy}
        \[
        \lvert \bdm{{s, \{s_i\}}} - H\left( s, \{s_i\}  \right) \leq
        \mathcal{O}{\left(
            l\left( s, s_i \right)
        \right)}
        \]
    \end{theorem}
    
    Finally, in \cite{DBLP:journals/corr/ZenilSKHR16}\cite{DBLP:journals/corr/abs-1211-1302}, the authors investigated Turing machines with $2$ symbols and up to $5$ states, and numerically verified that the approximation of algorithmic complexity given by either $\ctm$ and $\bdm$ mimics behaviors of algorithmic complexity. In other words, both $\ctm$ and $\bdm$ is shown to approximate algorithmic complexity given enough computing resources, and in the worse case gives results no worse than that estimated by common lossless compression algorithms.
    
\section{Experiment on Lossless Source Coding}\label{ch:exp}

    During investigation of methods approximating algorithmic complexity, we found lossless compression technique as algorithmic complexity estimator is considered useless, as illustrated in section \ref{ch:introduction}. Here we conducted some experiments on common lossless algorithms, investigating their real-world performance.

    The experiment is carried out on author's machine\ref{appendix:host_arch_machine}; code and experiment data at \cite{exp_ChuHanChen2021}\cite{paper_ChuHanChen2021}. Input data is \(2048\)-bytes for random source, and \(1024\)-bytes for \LaTeX code of this presentation; tested algorithms are \(\lzw\) and \(\lzma\). \(x\)-axis is original file size, while \(y\)-axis being compressed size.
    
    In the experimental results, one can see that though both \(\lzw\) and \(\lzma\) are to achieve optimality in Shannon entropy sense in asymptotic regime, they both have apparent deficiency for short strings: $\lzma$ have dictionary in the compressed data, which explains the large positive offset in compressed data length, making it unsuitable as estimator for algorithmic complexity; $\lzw$ gives worse compression rate than $\lzma$, implying room for improvement.
    
    Finally, as for compression using algorithmic probability, simple test results seem promising, in that in the calculator as by H. Zenil et al. \cite{OnlineCalc}, several tested results have their $\bdm$ complexity significantly lower than that given by $\text{gzip}$, which implements an variant of the \text{Deflate} algorithm, which in turn is combination of $\lzw$ and $\text{Huffman Code}$. However, real-world compression implementation based on $\bdm$ scheme seem irrealizable, since the huge amount of look-up table needed for finding exactly what Turing machine gives the string being considered amongst all the Turing machines $\ctm$ have run, and that the decoder needs same huge lookup-table to decode.
    
    \(\lzw\) with \(\text{C++}\) \(\text{rand()}\):
    \begin{figure}[hbt!]\label{appendix:exp_lzw_rnd}
        \caption{\(\lzw\) acting on \(\text{rand()}\)}
        \includegraphics[width=15cm]{exp_lzw_rnd.png}
        \centering
    \end{figure}

    \(\lzw\) with paper \LaTeX:
    \begin{figure}[hbt!]\label{appendix:exp_lzw_normal}
        \caption{\(\lzw\) acting on \LaTeX}
        \includegraphics[width=15cm]{exp_lzw_normal}
        \centering
    \end{figure}

    \(\lzma\) with \(\text{C++}\) \(\text{rand()}\):
    \begin{figure}[hbt!]\label{appendix:exp_lzma_rnd}
        \caption{\(\lzma\) acting on     \(\text{rand()}\)}
        \includegraphics[width=15cm]{exp_lzma_rnd}
        \centering
    \end{figure}

    \(\lzma\) with paper \LaTeX:
    \begin{figure}[hbt!]\label{appendix:exp_lzma_normal}
        \caption{\(\lzma\) acting on \LaTeX}
        \includegraphics[width=15cm]{exp_lzma_normal}
        \centering
    \end{figure}
    
    \(\bdm\) versus \text{gzip} for string \(01001000100001111011110\):
    \begin{figure}[hbt!]\label{appendix:exp_bdm_gzip_exp_1}
        \caption{\(\bdm\) versus \text{gzip} for string \(01001000100001111011110\)}
        \includegraphics[width=15cm]{exp_result_png/bdm_1.png}
        \centering
    \end{figure}
    
    \(\bdm\) versus \text{gzip} for string \(0011010011010100101101\):
    \begin{figure}[hbt!]\label{appendix:exp_bdm_gzip_exp_2}
        \caption{\(\bdm\) versus \text{gzip} for string \(0011010011010100101101\)}
        \includegraphics[width=15cm]{exp_result_png/bdm_2.png}
        \centering
    \end{figure}
    
    
\section{Appendices}\label{ch:app}

\subsection{Platform 1 Specification}\label{appendix:host_wsl_machine}
\begin{lstlisting}[style=shell]
$ uname -srv
Debian GNU/Linux 10 (buster) on Windows 10 x86\_64, 4.19.128-microsoft-standard
$ gcc -v
gcc version 10.2.1 20210110 (Debian 10.2.1-6)
$ g++ --std=c++11 -Wall -O2 -march=native -mtune=native
\end{lstlisting}
The binary is \(17320\) bytes.

\section{Platform 2 Specification}\label{appendix:host_arch_machine}
\begin{lstlisting}[style=shell]
$ uname -srv
Linux 5.12.12-arch1-1 #1 SMP PREEMPT Fri, 18 Jun 2021 21:59:22 +0000
$ clang -v
clang version 12.0.0
$ clang++ --std=c++11 -Wall -O2 -march=native -mtune=native
\end{lstlisting}

\section{Contribution}\label{ch:contrib}

\begin{itemize}
    \item {\textbf{Chi-Tse Huang}} \(\ctm\) and \(\bdm\) reseach, 50\%
    \item {\textbf{Chu-Han Chen}} Turing machine and properties of algorithmic complexity, experiment conduction, 50\%
\end{itemize}

\section{References}

\printbibliography[heading=none]

\end{document}
